# Reservoir Sampling

Task
-----------
Write a command line script that reads from stdin. When it terminates, it should print a sample, uniformly at random, of 10 of the input lines. (That is, each line should have equal chance of being in the sample).  You should be able to run this script any number of times, including concurrently. When a given instance of the script terminates, it should print the 10 line sample of every line that *any* instance of it has ever seen so far.

To run stateless
-------
Example: Generate sample size 10 between 1 and 100

 ```$ for i in {1..100}; do echo $i; done | ./sbt "run 10"```


To run stateful
-------
1. Install Redis (http://download.redis.io/releases/redis-3.0.2.tar.gz).
2. Start redis-server.

   ```$ src/redis-server```
3. Run (as above) with extra flag to denote "distributed" mode (e.g 'd')

   ```$ for i in {1..100}; do echo $i; done | ./sbt "run 10 d"```