package sampling

import com.redis.RedisClient

/**
 * This application reads data from stdin and prints a sample, uniformly at random, of K of the input lines.
 * When run multiple times or in a distributed setting, the K line sample should be derived from elements that
 * have been seen by every instance.
 */
object ReservoirSampling extends App {

  // define K sample size (default is 10)
  val sampleSize = args.headOption.getOrElse("10").toInt
  val collector = new SampleCollector[Int](sampleSize)

  // read from stdin and print sample results
  if (args.length <= 1) collector.collect(io.Source.stdin.getLines().map(_.toInt).toSeq).foreach(printSamples)
  else {
    // configure default redis client
    val redisClient = new RedisClient("localhost", 6379)
    collector.distributedCollect(
      io.Source.stdin.getLines().map(_.toInt).toSeq,
      redisClient
    ).foreach(printSamples)
  }

  def printSamples(output: Seq[String]) = println(s"Sample: ${output.mkString(",")}")
}
