package sampling

import com.redis.RedisClient

import scala.collection.mutable.ArrayBuffer
import scala.util.Random

/**
 * Generate a randomly uniform sample of size K.  When run in a distributed environment, each partition
 * sends the top K sorted items (based on a random id/hash) to a central reducer that extracts the top
 * K over all elements.  When run over a single sequence, a popular O(n) algorithm is used as described
 * here: http://en.wikipedia.org/wiki/Reservoir_sampling
 *
 * @tparam T type of data to collect
 * @param k size of the sample
 */
class SampleCollector[T](k: Int) {

  // Redis sorted set id
  val TopK = "topk-samples"

  // Random number generator
  val Rng = new Random(System.currentTimeMillis)

  /**
   * Generate a random uniform sample of data from a given sequence.
   *
   * @param input input data
   * @return Optional list of samples
   */
  def collect(input: Seq[T]): Option[Seq[String]] = {
    // create a reservoir with sample size
    val reservoir = ArrayBuffer[T]()

    input.zipWithIndex.foreach { case (ln, i) =>
      if (i < k) reservoir.append(ln)
      else {
        val rand = Rng.nextInt(i + 1)
        if (rand < k) {
          reservoir(rand) = ln
        }
      }
    }
    Option(reservoir.toSeq.map(_.toString))
  }

  /**
   * Generate a random uniform sample of data from a given sequence in a distributed manner.
   * The approach is to associate each element with a random number between 0 and 1
   * and emits the top K elements (largest) to an external reducer for overall top K.
   *
   * This is based on the idea that a random sample of size K is equivalent
   * to a random permutation (ordering) of elements.
   *
   * @param input input data
   * @return Optional list of samples
   */
  def distributedCollect(input: Seq[T], redisClient: RedisClient): Option[Seq[String]] = {
    // default is max heap - keeps track of k elements with the largest ids
    val results = scala.collection.mutable.PriorityQueue.empty(Ordering.by((_: (Float, T))._1))

    // associate a random ID to each element
    Seq.fill(input.size)(Rng.nextFloat())
      .zip(input.toSeq)
      .foreach(results.enqueue(_))

    // emit only top K elements to reducer
    // note: dequeueAll preserves order
    results.dequeueAll.take(k).foreach(elem => redisClient.zadd(TopK, elem._1, elem._2))

    // return K samples
    redisClient.zrange(TopK, 0, k - 1, RedisClient.DESC)
  }
}
