name := "reservoir"

version := "1.0"

libraryDependencies ++= Seq(
  "net.debasishg" %% "redisclient" % "3.0"
)

scalaVersion := "2.11.6"
